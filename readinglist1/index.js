/* B290 ACADEMIC BREAK READING LIST: JAVASCRIPT ACTIVITY */

// console checker
//console.log("JavaScript Test");



/* ===== A. JAVASCRIPT - FUNCTION PARAMETERS AND RETURN STATEMENT  ===== */
/* ===== B. JAVASCRIPT - SELECTION CONTROL STRUCTURES  ===== */

// Simple Shipping Function
function shipItem(item, weight, location) {

    // new variable for total shipping fee    
    let totalShippingFee = 0;

    // weight in kilograms: 3kg, 5kg, 8kg, 10kg, 13kg 
    if (weight <= 3 || weight < 5 ) {
        totalShippingFee = 150;
    } else if (weight >= 5 && weight < 8) {
        totalShippingFee = 280;
    } else if (weight >= 8 && weight < 10) {
        totalShippingFee = 340;
    } else if (weight >= 10 && weight < 13) {
        totalShippingFee = 410;
    } else if (weight >= 13) {
        totalShippingFee = 560;
    } else {
        console.log("Invalid weight input. Try again.");
        totalShippingFee = undefined;
    };

    // location only accepts local and international (lowercase and sentence case)
    switch (location) {
        case "local":
        case "Local":
            // do nothing
            break;
        case "international":
        case "International":
            totalShippingFee += 250;
            break;
        default:
            console.log("Invalid location input. Try again.");
            break;
    }

    // return message with conversion of item to string
    return "The total shipping fee for a/an " + String(item) + " that will ship to " + location + " is P" + totalShippingFee;

}

/* ===== END OF JAVASCRIPT - FUNCTION PARAMETERS AND RETURN STATEMENT  ===== */
/* ===== END OF JAVASCRIPT - SELECTION CONTROL STRUCTURES  ===== */





/* ===== C. JAVASCRIPT - OBJECTS ===== */
/* ===== D. JAVASCRIPT - ARRAY MANIPULATION ===== */

// Product Object Constructor
function Product(name, price, quantity) {

    // properties
    this.name = name;
    this.price = price;
    this.quantity = quantity;

}

// Customer Object Constructor
function Customer(name, cart) {

    // properties
    this.name = name;
    this.cart = cart;

    // methods
    this.addToCart = function(product) {

        // check if product is already added to cart
        if (this.cart.includes(product)) {
            return "Product already added to cart.";
        } else {
            // In case (for square bracket notation): this.cart[product];
            this.cart.push(product);
            return product + " is added to cart.";
        }

    };
    this.removeToCart = function(product) {

        // check if cart is empty or not
        if (this.cart.length === 0) {
            return "Cart empty. Add to cart first.";
        } else {
            // check if product exists in cart
            if (this.cart.includes(product)) {
                // In case (for square bracket notation): this.cart[product];
                let productToRemove = this.cart.indexOf(product);
                this.cart.splice(productToRemove, 1);
                return product + " is removed from the cart.";
            } else {
                return "Product does not exist in cart.";
            }
        }

    };
}

// Instances of Product and Customer
let product1 = new Product("Lenovo Yoga 6", 40998, 1);
let product2 = new Product("Sony Inzone H3", 3999, 3);
let myCustomer = new Customer("Leonore Kalaw", [product1.name]);

// Invocation test scripts (HIDDEN)
/*
    // adding to cart
    myCustomer.addToCart(product2.name);
    console.log(myCustomer.cart);

    // removing from cart
    myCustomer.removeToCart(product2.name);
    console.log(myCustomer.cart);
*/

/* ===== END OF JAVASCRIPT - OBJECTS ===== */
/* ===== END OF JAVASCRIPT - ARRAY MANIPULATION ===== */